const express      = require('express');
const morgan       = require('morgan');
const cors         = require('cors');
const router = require('./router');
const app = express();

// Utilise cors pour autoriser tous serveur entrant
app.use(cors({
    exposedHeaders: ['Authorization'],
    origin: '*'
}));
// Morgan sert à avoir des log request plus complet
app.use(morgan('dev'));
// Configuration pour prendre en charge les json en requête et retour
app.use(express.json());

// Appel du router défini dans le fichier
app.use('/', router);

// Définition du port d'écoute
app.listen(8000);