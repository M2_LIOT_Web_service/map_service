const express = require("express");
const router = express.Router();
const mapService = require("./service/mapService");

router.get('/map', (req,res) => {
    res.send('accueil Map');
});

// redirection dans le cas ou l'url est /
router.use('/', (req,res, next) => {
    if(req.url == '/'){
        res.redirect('/map');
    } else {
        next();
    }
});

router.get('/map/getItinerary/:xStart/:yStart/:xEnd/:yEnd', mapService.GetItinerary);

router.get('/map/getItineraryFaster/:xStart/:yStart/:xEnd/:yEnd', mapService.GetItineraryFaster);

router.get('/map/getItineraryWithoutPlace/:startAdress/:endAdress/:strategicPoint', mapService.GetItineraryWithoutPlace);

router.get('/map/getItineraryFasterWithoutPlace/:startAdress/:endAdress/:place', mapService.GetItineraryFasterWithoutPlace);

router.get('/map/getItineraryWithPlace/:startAdress/:endAdress/:place', mapService.GetItineraryWithPlace);

router.get('/map/getItineraryFasterWithPlace/:startAdress/:endAdress/:place', mapService.GetItineraryFasterWithPlace);

router.use((req,res) => {
    res.status(404).send('Page introuvable !');
});

module.exports = router;